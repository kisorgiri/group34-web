// ECMAScript || es

// es6(es2015) ==> modern JS
// react and front end
// ES6
// 1. object short hand
// 2. object destruct
// 3. arrow notation function
// 4.import and export
// 5.default argument
// 6. Rest and Spread operator
// 7.template literals
// 8. class

// 1. Object Short hand
var phone = 333;

var obj = {
    name: 'broadway',
    email: '33l@gmal.com',
    phone
}

console.log('obj >>', obj)

// function a(arg) {
//     console.log('arg is >>', arg)

// }
// a({ phone })

function getSomething() {
    return {
        fruits: 'apple',
        vegitables: 'potato',
        pencils: 'pencils',
        abc: 'xyz'
    }
}

var result = getSomething();
console.log('result >>', result)

// object destruct
var { fruits, pencils } = getSomething();
console.log('fruits >>', fruits)
console.log('pencils >>', pencils)

const { name: Kishor } = {
    name: 'broadway',
    address: 'tinkune',
    phone: '333',
    status: 'online'
}

console.log('name is >>', Kishor)



// 3. arrow notation function
// function askMoney(amt, abca) {

// }

// const askMoney = (amt) => {
//     console.log('inside ask money',amt)
// }

// askMoney('broadway')


// if we have single argument parenthesis is optional
const askMoney = amt => {
    console.log('inside ask money', amt)
}
askMoney('broadway')

// return type
// function abc(xyz) {
//     return 'hello ' + xyz;
// }
// const abc = xyz => {
//     return 'hello ' + xyz
// }

// one liner function
// const abc = () => 'hello '

// var ressss = abc('kishor')

// console.log('ress >>', ressss)


var bikes = [
    {
        name: 'pulsar',
        brand: 'bajaj',
        price: 333,
        color: 'white'
    },
    {
        name: 'xr',
        brand: 'honda',
        price: 333,
        color: 'red'
    },
    {
        name: 'duke',
        brand: 'ktm',
        price: 333,
        color: 'red'
    },
    {
        name: 'avenger',
        brand: 'bajaj',
        price: 333,
        color: 'black'
    },
    {
        name: 'crf',
        brand: 'honda',
        price: 333,
        color: 'red'
    }
]

// hondabikes
var hondabikes = bikes.filter(function (item) {
    if (item.brand === 'honda') {
        return true;
    }
})

var hondaBikes1 = bikes.filter(item => item.brand === 'honda');

console.log('honda bikes is >>', hondabikes)
console.log('honda bikes  1 is >>', hondaBikes1)


// arrow notation function advantages
// arrow notation function will inherit parent this

// TODO bind,apply,call ===> to pass context in functions
// import and export
// export
// there are two ways of export in es6
// named export
// syntax
// export const App = 'app';
// export class Fruits {

// }
// there can be multiple named export in a file
// export const newName = App;

// default export
// syntax
// export default <anything>'array,object,sting,number,boolean'
// eg
// const a = 'i am a';
// export default a;
// there can be only one default export in a file
// * there can be both named and default export

// import
// import totally depends on how it is exported
// if named export
// syntax
// import {exported_name,another name} from './file location';

// if default export
// syntax
// import anyName from './location of file'

// if both
// import {name1,name2}, anyName from 'location of file'

// es5(es2014)==> stable JS
// till now JS and backend

// default argument
function register(emailAddress = 'jskiosr@gmail.com') {
    console.log('arg is >>', emailAddress)
}

// function action(data = []) {
//     // array related stuff assuming data is array
// }

function action(details = {}) {
    console.log('details name ', details.name)
    // array related stuff assuming data is array
}

action();


// pre-requisities for react
// JSX
// es6
// babel or compiler
// task runner(gulp,grunt)
// webpack (module bundler)


// setTimeout(()=>{

// },88)


// spread operator
// syntax
// ...
// value spread 
var remote = {
    name: 'dell',
    color: 'black',
    price: 333
}
var remote1 = {
    ...remote
};

var fruits = {
    name: 'apple',
    color: 'red',
    origin: 'nepal'
}

var vegitables = {
    name: 'potato',
    color: 'gray',
    origin: 'test',
    price: 222,
    status: 'sold',
    acd: 'xyz'
}

var fruitsNvegitables = {
    ...vegitables,
    ...fruits,
}
console.log('fruits and vegitables', fruitsNvegitables)

remote1.name = 'projector remote';
console.log('remote >>', remote)
console.log('remote 1', remote1)

// rest operator
// ...

// object destruction
var status = 'online'
var { status: myStatus, ...kishor } = fruitsNvegitables;
console.log('status is >>', status);
console.log('my status is >>', myStatus);
// console.log('price is >>', price)
console.log('rest is >>>', kishor)


// template literals
// string concatination

var text = 'welcome';
var place = 'broadway'
function welcome(name) {
    var greet = 'hi ' + name + ' ' + text + ' to ' + place
    // es6
    var greet1 = `hi ${name}, ${text} to ${place}`
    console.log('greeet is >>', greet)
    console.log('greeet 1 is >>', greet1)
}

welcome('ravi');


// async await
// async function test() {
//     const result = await callAPI();
//     // code execution

// }

// class
// class is group of constructor, methods and fields(properties)
// user define data type

// eg
// class Students {
//     private roll_no; // properties
//     constructor() { // constructor
//         this.roll_no = 33;
//         // constructor is used to initilize values
//     }
//     // function keyword is not necessary inside class
//   public  getRoll_no = () => { //methods

//     }

//    protected setRoll_No() {

//     }
// }


// class OStu extends Students {
//     constructor() {
//         super();
//         // super call is parent class constructor call
//     }
//     getName(){

//     }
// }

// var bijay = new OStu();
// // bijay.
// var ram = new Students();
// var shyam = new Students();

// // var abc=  new Array();
// // abc.


// pillers of OOP
// inheritance
// abstraction
// polymorphism
// encapsulation
// access specifiers (private,public,protected)
// private ==> within class
// public==> within class , instance and child
// protected ==> within class and child only

// var fruits = ['apple', 'banana', 'orange'];
// const { length, indexOf } = fruits;

// console.log('length >.', length)


// laptops.forEach(({name})=>{

// })
