// redux is state management tool

// Web architecture
// MVC ===> 
// concerns  ===> Models Views and Controller
// bidirectional communication between models and controller


// FLUX architecture
// concerns ==> views, Actions, Dispatchers, Store
// unidirectional data flow 
// Views ===> Actions ===> dispatchers ===> store

// there can be multiple store
// store holds the logic update data

// REDUX on top flux 
// reduce complexity with flux
// concerns ==> Views, Actions, Reducers , Store
// unidirectional data flow
// there should single store
// single source of truth
// reducers holds logic to update store
// Views ====> Actions ===> Reducers ===> store ==> contains state ==> state defines UI


// react with redux 
// prerequisites
// library
// redux
// react-redux
// middleware  ==> redux-thunk, redux-saga
