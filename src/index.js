import React from 'react';
import ReactDOM from 'react-dom'
import App from './components/App.component'

const container = document.getElementById('root');

ReactDOM.render(<App />, container)

// component 
// component is basic building block for react
// component always returns single html node
// component can be
// stateless component 
// statefull component

// component can be written as
// class based component
// functional component 

// >17
// before 16.8
// ------> class based component for statefull
// ------> functional component for stateless

// now (> 16.8)
// ---> functional component can be statefull as well as stateless
// hooks 

// glossary
// state
// props
// state and props both are component data

// props ==> incoming data for an component 
// state ==> data within a component ==> class based component

// using react we create our own element and define our own attributes
