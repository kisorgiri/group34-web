import { combineReducers } from 'redux';
import { ProductReducer } from './products.red';

// import userReducer form './userred';
// import notification form './notification';

export const RootReducer = combineReducers({
    product: ProductReducer,
    user: {},
    notification: {}
})
