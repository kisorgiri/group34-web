import { PRODUCTS_RECEIVED, PRODUCT_REMOVED, SET_ISLOADING, SET_PAGE_NUMBER } from "../actions/products/type"

const defaultState = {
    isLoading: false,
    products: []
}

export const ProductReducer = (state = defaultState, action) => {
    console.log('at reducer .>', action)
    // TODO update state according to action

    switch (action.type) {

        case SET_ISLOADING:
            return {
                ...state,
                isLoading: action.payload
            }
        case PRODUCTS_RECEIVED:
            return {
                ...state,
                products: action.payload
            }
        case PRODUCT_REMOVED:
            const { products } = state;
            products.forEach((item, index) => {
                if (item._id === action.productId) {
                    products.splice(index, 1);
                }
            })
            return {
                ...state,
                products: [...products]
            }
        case SET_PAGE_NUMBER:
            return {
                ...state,
                pageNumber: action.payload
            }
        case 'PRODUCT_RECEIVED':
            return {
                ...state,
                product: action.payload
            }

        case 'SET_ISSUBMITTING':
            return {
                ...state,
                isSubmitting: action.payload
            }

        case 'REVIEW_ADDED':
            return {
                ...state,
                product: action.payload
            }

        default:
            return {
                ...state
            }
    }

}
