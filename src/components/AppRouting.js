import React from 'react';

import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { ForgotPassword } from './Auth/ForgotPassword/ForgotPassword.component';
import { Login } from './Auth/Login/Login.component';
import { Register } from './Auth/Register/Register.component';
import { ResetPassword } from './Auth/ResetPassword/ResetPassword.component';
import { Header } from './Common/Header/Header.component';
import { Sidebar } from './Common/Sidebar/Sidebar.component';
import { AddProduct } from './Products/AddProduct/AddProduct.component'
import { EditProduct } from './Products/EditProduct/EditProduct.componet';
import ProductDetailsLanding from './Products/ProductDetailsLanding/ProductDetails.landing';
import { SearchProduct } from './Products/SearchProduct/SearchProduct.component';
import { ViewProducts } from './Products/ViewProducts/ViewProducts.component';
import { MessageComponent } from './Users/Messages/Message.component';
const Home = (props) => {
    console.log('props in home ', props);
    return (
        <p>Home Page</p>
    )
}
const Dashboard = (props) => {
    console.log('props in Dashboard ', props);
    return (
        <p>Dashboard Page</p>
    )
}

const About = (props) => {
    console.log('props in About ', props);
    return (
        <p>About Page</p>
    )
}

const Settings = (props) => {
    console.log('props in Settings ', props);
    return (
        <p>Settings Page</p>
    )
}

const NotFound = (props) => {
    // console.
    return (
        <div>
            <h2>Not Found</h2>
            <img width="400" src="./images/notfound.jpg" alt="not_found.png"></img>
        </div>
    )
}

const ProtectedRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={(routeProps) => (
        localStorage.getItem('token')
            ? <>
                <Header isLoggedIn={true}></Header>
                <Sidebar isLoggedIn={true} />
                <div className="main-content">
                    <Component {...routeProps} ></Component>
                </div>
            </>
            : <Redirect to="/" ></Redirect> //TODO add props from where it is redirected
    )}>

    </Route >

}
const PublicRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={(routeProps) => (
        <>
            <Header isLoggedIn={localStorage.getItem('token') ? true : false}></Header>
            <Sidebar isLoggedIn={localStorage.getItem('token') ? true : false} />

            <div className="main-content">
                <Component {...routeProps} ></Component>
            </div>
        </>
    )}>

    </Route >

}
const AuthRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={(routeProps) => (
        <>
            <Header />
            <div className="main-content">
                <Component {...routeProps} ></Component>
            </div>
        </>
    )}>

    </Route >

}


export const AppRouting = (props) => {
    return (
        <BrowserRouter>
            <Switch>
                <AuthRoute
                    path="/"
                    exact
                    component={Login}
                ></AuthRoute>
                <AuthRoute path="/register" component={Register} />
                <AuthRoute path="/forgot_password" component={ForgotPassword} />
                <AuthRoute path="/reset_password/:token" component={ResetPassword} />
                <ProtectedRoute path="/about" component={About} />
                <ProtectedRoute path="/settings" component={Settings} />
                <ProtectedRoute path="/home/:name" component={Home}></ProtectedRoute>
                <ProtectedRoute path="/dashboard" component={Dashboard}></ProtectedRoute>
                <ProtectedRoute path="/add_product" component={AddProduct}></ProtectedRoute>
                <ProtectedRoute path="/view_products" component={ViewProducts}></ProtectedRoute>
                <ProtectedRoute path="/edit_product/:id" component={EditProduct}></ProtectedRoute>
                <ProtectedRoute path="/product_details/:id" component={ProductDetailsLanding}></ProtectedRoute>
                <ProtectedRoute path="/message" component={MessageComponent}></ProtectedRoute>
                <PublicRoute path="/search_product" component={SearchProduct}></PublicRoute>
                <PublicRoute component={NotFound}></PublicRoute>
            </Switch>
        </BrowserRouter>
    )
}


// Route defined component will always have three props
// history  ==> functions for navigation
// match  ==> dynamic url ko value
// location ==> routing change huda pathiyeko data access ko lagi ani optional url value (?)


// revision
// library ==> react router dom
// BrowserRouter ==> wrapper for routing configuration
// Route => configuration
// Route will supply history match and location props to its component
// Exact attribute ==> ensure exact path match
// switch ==> incase of path match empty config lai prevent
// Link, NavLink ===> click event ma navigation
// history ==> for navigation 
// match for dynamic value [dynamic routing configuration]
// location for data
