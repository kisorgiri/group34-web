import React, { Component } from 'react'
import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css";
import { relativeTime } from '../../../../utils/dateUtil';
const IMG_URL = process.env.REACT_APP_IMG_URL;


const images = [
    {
        original: 'https://picsum.photos/id/1018/1000/600/',
        thumbnail: 'https://picsum.photos/id/1018/250/150/',
    },
    {
        original: 'https://picsum.photos/id/1015/1000/600/',
        thumbnail: 'https://picsum.photos/id/1015/250/150/',
    },
    {
        original: 'https://picsum.photos/id/1019/1000/600/',
        thumbnail: 'https://picsum.photos/id/1019/250/150/',
    },
];
export default class ProductDetails extends Component {
    constructor() {
        super()

    }
    componentDidMount() {
        console.log('check products >>', this.props)
    }

    getImages = () => {
        return (this.props.product.images || []).map((img, index) => ({
            original: `${IMG_URL}/${img}`,
            thumbnail: 'https://picsum.photos/id/1018/250/150/'
        }))
    }

    render() {
        const { product } = this.props;
        return (
            <>
                <div className="row">
                    <div className="col-md-6">
                        <ImageGallery items={this.getImages()} />
                    </div>
                    <div className="col-md-6">
                        <h2>{product.name}</h2>
                        <p>{product.description}</p>
                        <p>...</p>
                        {
                            product.reviews && product.reviews.length > 0 && (
                                <>
                                    <p>Reviews</p>
                                    {product.reviews
                                        .filter(review => review)
                                        .map((review, index) => (
                                            <div key={index}>
                                                <p>{review.point}</p>
                                                <p>{review.message}</p>
                                                <p> <strong>{review.user.username}</strong></p>

                                                <small>{relativeTime(review.createdAt)}</small>


                                            </div>
                                        ))}
                                </>
                            )
                        }
                    </div>
                </div>
            </>
        )
    }
}
