import React, { Component } from 'react'
import { SubmitButton } from './../../../Common/SubmitButton/SubmitButton.component'
export default class ReviewForm extends Component {
    constructor() {
        super()

        this.state = {
            data: {
                reviewPoint: '',
                reviewMessage: ''
            }
        }
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }))
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.addReview(this.props.productId, this.state.data)
        this.setState({
            data: {
                reviewPoint: '',
                reviewMessage: ''
            }
        })
    }



    render() {
        return (
            <>
                <h2>Add Reivew</h2>
                <form onSubmit={this.handleSubmit} className="form-group" noValidate>
                    <label>Point</label>
                    <input type="number" value={this.state.data.reviewPoint} onChange={this.handleChange} className="form-control" name="reviewPoint" ></input>
                    <label>Message</label>
                    <input type="text" value={this.state.data.reviewMessage} onChange={this.handleChange} className="form-control" name="reviewMessage" placeholder="message here" ></input>
                    <hr />
                    <SubmitButton
                        isSubmitting={this.props.isSubmitting}
                    >
                    </SubmitButton>
                </form>
            </>
        )
    }
}
