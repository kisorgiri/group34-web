import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { notify } from '../../../utils/toastr';
import { handleError } from '../../../utils/errorHandler';
import { httpClient } from '../../../utils/httpClient';
import { SubmitButton } from '../../Common/SubmitButton/SubmitButton.component';

const defaultForm = {
    name: '',
    username: '',
    password: '',
    confirmPassword: '',
    email: '',
    phoneNumber: '',
    dob: '',
    gender: '',
    temporaryAddress: '',
    permanenetAddress: ''
}
const errorFields = {
    username: false,
    password: false,
    email: false,
    confirmPassword: false
}
export class Register extends Component {
    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...errorFields
            },
            isValidForm: false,
            isSubmitting: false,
        }
        // console.log('constructor at first')
    }
    // init
    componentDidMount() {
        // it will be invoked only once
        // console.log('i will be self invoked once component is fully loaded')
        // purpose
        // data preparation
        // ====> API call
        // state modification

    }

    // update stage
    // once props or state is modified component will be update
    // render
    // componentDidUpdate(preProps, preState) {
    //     // console.log('prevState >>', preState.data)
    //     // console.log('current state >>', this.state.data)
    //     // console.log('once state or props is changed')
    //     // check difference and perfrom necessary actions
    // }

    // destory
    componentWillUnmount() {
        console.log('once usage is over');
    }

    handleSubmit = e => {
        e.preventDefault();
        const isValidForm = this.validateRequiredFields();
        if (!isValidForm) {
            return;
        }
        this.setState({
            isSubmitting: true
        })
        // data is ready
        // API call
        httpClient
            .POST(`/auth/register`, this.state.data)
            .then(response => {
                notify.showInfo("Registration successfull please login!")
                this.props.history.push('/');
            })
            .catch(err => {
                handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    onChange = e => {
        let { name, value } = e.target;
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name)
        })
    }
    // regex validation
    // pattern match

    // determine the event either change or submit
    validateForm = (fieldName, evt) => {
        let errMsg;
        switch (fieldName) {
            case 'username':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : 'required field*';
                break;

            case 'password':
                errMsg = this.state.data[fieldName]
                    ? this.state.data['confirmPassword']
                        ? this.state.data['confirmPassword'] === this.state.data[fieldName]
                            ? ''
                            : 'password did not match'
                        : this.state.data[fieldName].length > 6
                            ? ''
                            : 'weak password'
                    : 'required field*'
                break;
            case 'confirmPassword':
                errMsg = this.state.data[fieldName]
                    ? this.state.data['password']
                        ? this.state.data['password'] === this.state.data[fieldName]
                            ? ''
                            : 'password did not match'
                        : this.state.data[fieldName].length > 6
                            ? ''
                            : 'weak password'
                    : 'required field*'
                break;
            case 'email':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].includes('@') && this.state.data[fieldName].includes('.com')
                        ? ''
                        : 'invalid email'
                    : 'required field*'
                break;

            default:
                break;
        }

        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);
            console.log('errors is >>', errors)
            // const filteredError = errors.filter(function (item) {
            //     if (item) {
            //         return item;
            //     }
            // })
            // console.log('filtered error >>', filteredError)
            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    validateRequiredFields = () => {
        let validForm = true;
        let usernameErr = false;
        let passwordErr = false;
        let confirmPasswordErr = false;
        let emailErr = false;

        if (!this.state.data.username) {
            validForm = false
            usernameErr = 'required field*';
        }
        if (!this.state.data.password) {
            validForm = false
            passwordErr = 'required field*';
        }
        if (!this.state.data.confirmPassword) {
            validForm = false
            confirmPasswordErr = 'required field*';
        }
        if (!this.state.data.email) {
            validForm = false
            emailErr = 'required field*';
        }

        this.setState({
            error: {
                username: usernameErr,
                password: passwordErr,
                confirmPassword: confirmPasswordErr,
                email: emailErr
            }
        })

        return validForm;
    }

    render() {
        return (
            <div className="auth-box">
                <h2>Register</h2>
                <p>Please provide necessary details to register</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Name</label>
                    <input type="text" className="form-control" name="name" placeholder="Name" onChange={this.onChange} />
                    <label>Username</label>
                    <input type="text" className="form-control" name="username" placeholder="Username" onChange={this.onChange} />
                    <p className="error">{this.state.error.username && this.state.error.username}</p>
                    <label>Password</label>
                    <input type="password" className="form-control" name="password" placeholder="Password" onChange={this.onChange} />
                    <p className="error">{this.state.error.password && this.state.error.password}</p>

                    <label>Confirm Password</label>
                    <input type="password" className="form-control" name="confirmPassword" placeholder="Confirm Password" onChange={this.onChange} />
                    <p className="error">{this.state.error.confirmPassword && this.state.error.confirmPassword}</p>

                    <label>Email</label>
                    <input type="text" className="form-control" name="email" placeholder="Email" onChange={this.onChange} />
                    <p className="error">{this.state.error.email && this.state.error.email}</p>

                    <label>Phone Number</label>
                    <input type="number" className="form-control" name="phoneNumber" onChange={this.onChange} />
                    <label>Gender</label>
                    <br />
                    <input type="radio" value="male" name="gender" onChange={this.onChange} /> Male
                    <input type="radio" value="female" name="gender" onChange={this.onChange} /> Female
                    <input type="radio" value="others" name="gender" onChange={this.onChange} /> Others
                    <br />
                    <label>D.O.B</label>
                    <input type="date" className="form-control" name="dob" onChange={this.onChange} />
                    <label>Temporary Address</label>
                    <input type="text" className="form-control" name="temporaryAddress" placeholder="Temporary Address" onChange={this.onChange} />
                    <label>Permanent Address</label>
                    <input type="text" className="form-control" name="permanentAddress" placeholder="Permanent Address" onChange={this.onChange} />
                    <hr />
                    <SubmitButton
                        isSubmitting={this.state.isSubmitting}
                        isDisabled={!this.state.isValidForm}
                    >

                    </SubmitButton>
                </form>
                <p>Already Registered? <Link to="/">back to login</Link></p>
            </div>
        )
    }
}

//component life cycle stage

// init
// ===> constructor,render
// componentDidMount();

// change
// either state change or props change
// componentDidUpdate();

// destory
// component life cycle is over
// componentWillUnMount();
