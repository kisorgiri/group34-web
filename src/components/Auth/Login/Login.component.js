import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { notify } from './../../../utils/toastr'
import { handleError } from '../../../utils/errorHandler';
import { httpClient } from '../../../utils/httpClient';
import { SubmitButton } from '../../Common/SubmitButton/SubmitButton.component';

// class based component
// statefull component

// syntax
// class ClassName extends React.Component { 
// constructor(){
//     super()
// }
// render(){}
// }


const defaultForm = {
    username: '',
    password: ''
}
export class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isSubmitting: false,
            isValidForm: false,
            remember_me: false
        }
    }
    // TODO life cycle hook and usage of props
    componentDidMount() {
        var rememberMe = JSON.parse(localStorage.getItem('remember_me'));
        if (rememberMe) {
            this.props.history.push('/dashboard')
        }
    }

    onSubmit = (e) => {

        e.preventDefault();
        let isValidForm = this.validateForm()
        if (!isValidForm) return;
        
        this.setState({
            isSubmitting: true
        })

        // API call
        httpClient
            .POST(`/auth/login`, this.state.data)
            .then((response) => {
                notify.showSuccess(`Welcome ${response.data.user.username}`);
                // localstorage setup
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('user', JSON.stringify(response.data.user))
                localStorage.setItem('remember_me', this.state.remember_me);

                // navigate to dashboard
                this.props.history.push('/dashboard')
                console.log('response is >>', response);

            })
            .catch(err => {
                handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
        // this.props.history.push('dashboard');

    }

    handleChange = (e) => {
        let { name, value, type, checked } = e.target;
        // console.log('name >>', name);
        // console.log('value is >>', value)
        if (type === 'checkbox') {
            return this.setState({
                remember_me: checked
            })
        }

        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            if (this.state.error[name]) {
                this.validateForm()
            }
        })
    }


    validateForm = () => {
        let usernameErr = this.state.data['username'] ? '' : 'required field*'
        let passwordErr = this.state.data['password'] ? '' : 'required field*'

        this.setState({
            error: {
                username: usernameErr,
                password: passwordErr
            }
        })

        let validForm = !(usernameErr || passwordErr)
        return validForm;
    }



    render() {
        // render method is mandatory inside class based component
        // render method must return single html node
        // try to keep UI logic inside render before return

        return (
            <div className="auth-box">
                <h2>Login</h2>
                <p>Please Login to Start Your Session</p>
                <form onSubmit={this.onSubmit} className="form-group">
                    <label htmlFor="username">Username</label>
                    <input type="text" className="form-control" value={this.state.username} name="username" id="username" onChange={this.handleChange} placeholder="Username"></input>
                    <p className="error">{this.state.error.username}</p>

                    <label htmlFor="password">Password</label>
                    <input type="password" className="form-control" value={this.state.password} name="password" id="password" onChange={this.handleChange} placeholder="Password"></input>
                    <p className="error">{this.state.error.password}</p>

                    <input type="checkbox" name="remember_me" onChange={this.handleChange}></input>
                    <label> &nbsp; Remember Me</label>
                    <br />
                    <SubmitButton
                        isSubmitting={this.state.isSubmitting}
                        enabledLabel="Login"
                        disabledLabel="Loging in..."
                    >

                    </SubmitButton>
                </form>
                <p>Don't have an Account?</p>
                <p style={{ float: 'left' }}>Register <Link to="/register">here</Link></p>
                <p style={{ float: 'right' }}><Link to="/forgot_password">forgot password?</Link></p>
            </div>
        )
    }
}
