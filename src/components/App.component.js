import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import { Provider } from 'react-redux'; // react redux is library to act as glue to redux store and react
// provider wraps the react component to supply store

import { AppRouting } from './AppRouting';
import { store } from '../store';

const App = () => {
    // functional component must have return block
    return (
        <div>
            <Provider store={store}>
                <AppRouting />
            </Provider>
            <ToastContainer />
        </div>

    )
}

export default App;
