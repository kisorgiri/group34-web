import React from 'react'
import { NavLink } from 'react-router-dom'
import './Sidebar.component.css';

export const Sidebar = (props) => {
    let content = props.isLoggedIn
        ? <div className="sidebar">
            <NavLink activeClassName="selected-sidebar" to="/add_product"> Add Product</NavLink>
            <NavLink activeClassName="selected-sidebar" to="/view_products">View Product</NavLink>
            <NavLink activeClassName="selected-sidebar" to="/search_product">Search Product</NavLink>
            <NavLink activeClassName="selected-sidebar" to="/notifications">Notification</NavLink>
            <NavLink activeClassName="selected-sidebar" to="/message">Messages</NavLink>
        </div>
        : null
    return content;
}
