import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { RootReducer } from './reducers';

const initialState = {
    product: {
        products: [],
        isLoading: false,
        pageNumber: 1,
        pageSize: 5,
        reviews: [],
        product: {},
        isSubmitting: false
    },
    // user: {
    //     data: [],
    //     isLoading: false,
    //     pageNumber: 1,
    //     pageSize: 10
    // },
    // notifications: {
    //     data: [],
    //     isLoading: false,
    //     pageNumber: 1,
    //     pageSize: 10
    // }
};
const middleware = [thunk]

export const store = createStore(RootReducer, initialState, applyMiddleware(...middleware))

// // store
// var centralizedState = {
//     proudcts: [],
//     users: [],
//     notifications: [],
//     isProductLoading: false,
//     isUsersLoading: false,
//     pageNumber: 0,
//     pageSize: 1
// }

// var centralizedState = {
//     product: {
//         data: [],
//         isLoading: false,
//         pageNumber: 1,
//         pageSize: 10,
//         reviews: [],
//     },
//     user: {
//         data: [],
//         isLoading: false,
//         pageNumber: 1,
//         pageSize: 10
//     },
//     notifications: {
//         data: [],
//         isLoading: false,
//         pageNumber: 1,
//         pageSize: 10
//     }
// }
